function navSlide() {
    const burger = document.querySelector(".burger");
    const nav = document.querySelector(".options");
    const navLinks = document.querySelectorAll(".options li");
    
    burger.addEventListener("click", () => {
        //Toggle Nav
        console.log("clicked");
        nav.classList.toggle("nav-active");
        
        //Animate Links
        navLinks.forEach((link, index) => {
            if (link.style.animation) {
                link.style.animation = ""
            } else {
                link.style.animation = `navLinkFade 0.125s ease forwards ${index / 7 + 0.5}s`;
            }
        });
        //Burger Animation
        burger.classList.toggle("toggle");

    });
    
}

navSlide();